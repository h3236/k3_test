---
- name: Install on master nodes
  hosts: master
  tasks:

    - name: Get vault token
      set_fact:
        vault_token: "{{ lookup('env', 'VAULT_TOKEN')}}"

    - name: Get vault address
      set_fact:
        vault_addr: "{{ lookup('env', 'VAULT_ADDR')}}"

    - name: Install k3 on master
      shell:
        cmd: "curl -sfL https://get.k3s.io | sh -"

    - name: Get node token file content
      slurp:
        src: /var/lib/rancher/k3s/server/node-token
      register: node_token_content
    
    - name: Set node token fact
      set_fact:
        node_token: "{{ node_token_content.content | b64decode | replace('\n', '') }}"
    
    - name: Fetch kube config file
      fetch:
        src: /etc/rancher/k3s/k3s.yaml
        dest: ~/kube_config
    
    - name: copy kube config to proper directory
      copy:
        src: ~/kube_config/k3-master-01/etc/rancher/k3s/k3s.yaml
        dest: ~/.kube/config
      delegate_to: localhost
      become: no
    
    - name: replace localhost with kube master
      lineinfile:
        path: ~/.kube/config
        regexp: "    server: https://127.0.0.1:6443"
        line: "    server: https://k3-master-01.homelab.com:6443"
      delegate_to: localhost
      become: no

- name: Install and configure worker nodes
  hosts: workers
  tasks:
    - name: Install and configure
      shell:
        cmd: "curl -sfL https://get.k3s.io | K3S_TOKEN={{ hostvars[groups['master'][0]]['node_token'] }} K3S_URL=https://{{ hostvars[groups['master'][0]]['ansible_fqdn'] }}:6443 sh -"

- name: Install Dashboard
  hosts: localhost
  become: no
  tasks:
    - name: Add helm repo
      kubernetes.core.helm_repository:
        name: kubernetes-dashboard
        repo_url: https://kubernetes.github.io/dashboard/

    - name: Install Dashboard
      kubernetes.core.helm:
        name: kubernetes-dashboard
        chart_ref: kubernetes-dashboard/kubernetes-dashboard
        release_namespace: kubernetes-dashboard
        create_namespace: true

    - name: Create admin user
      kubernetes.core.k8s:
        state: present
        src: ../k8s/dashboard/admin_user.yml
    
    - name: Create dashboard ingress
      kubernetes.core.k8s:
        state: present
        src: ../k8s/dashboard/ingress.yml

- name: Configure the cluster for vault
  hosts: localhost
  become: no
  vars:
    vault_addr: "{{ lookup('env', 'VAULT_ADDR')}}"
    vault_token: "{{ lookup('env', 'VAULT_TOKEN')}}"

  tasks:
    - name: Install required python packages
      pip:
        name: '{{ item }}'
      loop:
        - hvac
        - ansible-modules-hashivault

    - name: Add Hashicorp repo
      kubernetes.core.helm_repository:
        name: hashicorp
        repo_url: https://helm.releases.hashicorp.com
    
    - name: Install Vault agent injector
      kubernetes.core.helm:
        name: vault
        chart_ref: hashicorp/vault
        release_namespace: vault
        create_namespace: true
        values:
          injector.externalVaultAddr: "{{ vault_addr }}"
    
    - name: Get Vault secret name
      shell:
        cmd: kubectl get secrets -n vault -o json | jq -r '.items[].metadata | select(.name|startswith("vault-token-")).name'
      register: vault_secret_name
    
    - name: Get JSON web token
      shell:
        cmd: "kubectl get secret -n vault {{ vault_secret_name.stdout }} -o json | jq '.data.token' | sed 's/\"//g' | base64 --decode"
      register: jwt_token
    
    - name: Get CA cert
      shell:
        cmd: kubectl config view --raw --minify --flatten --output='jsonpath={.clusters[].cluster.certificate-authority-data}' | base64 --decode
      register: ca_cert

    - name: Get master host
      shell:
        cmd: kubectl config view --raw --minify --flatten --output='jsonpath={.clusters[].cluster.server}'
      register: master_host
    
    - hashivault_auth_method:
        method_type: kubernetes

    - name: Create kubernetes auth method
      hashivault_k8s_auth_config:
        mount_point: kubernetes
        token_reviewer_jwt: "{{ jwt_token.stdout | replace('\\\"', '') }}"
        kubernetes_host: "{{ master_host.stdout }}"
        kubernetes_ca_cert: "{{ ca_cert.stdout }}"
        issuer: https://kubernetes.default.svc.cluster.local
    